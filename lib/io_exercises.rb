# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  random_number = 1 + rand(100)
  guess = nil
  guesses = 0
  until guess == random_number
    guesses+=1
    puts "guess a number"
    guess = gets.to_i
    puts "#{guess} too high" if guess>random_number
    puts "#{guess} too low" if guess<random_number
  end
  puts "You found the number #{random_number} in #{guesses} guess(es)"
end

def file_shuffler(file_name)
  file_contents = File.readlines(file_name)
  shuffled_contents = file_contents.shuffle
  f = File.open("#{file_name[0...-4]}-shuffled.txt","w")
  shuffled_contents.each do |line|
    f.puts line
  end
  f.close
end
